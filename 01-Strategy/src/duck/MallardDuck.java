package duck;

import behavior.FlyBehavior;
import behavior.FlyWithWings;
import behavior.Quack;

public class MallardDuck extends Duck {

	public MallardDuck() {
		this.flyBehavior = new FlyWithWings();
		this.quackBehavior = new Quack();
	}

	@Override
	public void display() {
		System.out.println("マガモです！");

	}

	public void setFlyBehavior(FlyBehavior fb) {
		this.flyBehavior = fb;
	}

}
