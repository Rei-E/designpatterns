package duck;

import behavior.FlyNoWay;
import behavior.Squack;

public class RubberDuck extends Duck {

	public RubberDuck() {
		this.flyBehavior = new FlyNoWay();
		this.quackBehavior = new Squack();
	}

	@Override
	public void display() {
		System.out.println("ゴム製の鴨です！");

	}

}
