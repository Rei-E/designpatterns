package duck;

import behavior.FlyBehavior;
import behavior.QuackBehavior;

public abstract class Duck {

	FlyBehavior flyBehavior;
	QuackBehavior quackBehavior;

	public void PerformFly() {

		flyBehavior.fly();
	}

	public void PerformQuack() {

		quackBehavior.quack();
	}

	public abstract void display();
}
