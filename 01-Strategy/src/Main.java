import behavior.FlyRocket;
import duck.MallardDuck;
import duck.RubberDuck;

public class Main {

	public static void main(String[] args) {

		// マガモ
		MallardDuck mallardDuck = new MallardDuck();
		mallardDuck.display();
		mallardDuck.PerformFly(); // 飛ぶ
		mallardDuck.PerformQuack(); // 鳴く

		System.out.println("------------------------------");

		// ゴム製の鴨
		RubberDuck rubberDuck = new RubberDuck();
		rubberDuck.display();
		rubberDuck.PerformFly(); // 飛ぶ
		rubberDuck.PerformQuack(); // 鳴く

		System.out.println("------------------------------");

		// マガモが飛行の振る舞い変更
		mallardDuck.setFlyBehavior(new FlyRocket());
		mallardDuck.display();
		mallardDuck.PerformFly(); // 飛ぶ

	}

}
